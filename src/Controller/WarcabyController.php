<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class WarcabyController extends Controller
{
    protected $className = 'WarcabyController';

    /**
     * @Route("/warcaby/pdf/{params}", name="app_warcaby_pdf")
     * @param $params
     */
    public function pdf($params)
    {
        $paramObj = json_decode($params);

        $board = $this->initializeBoard();
        $board = $this->parsePositions($board, $paramObj->positions);

        $boardParms = $this->parseBoardParams($paramObj);

        $html = $this->renderView(
            'warcaby.html.twig', ['board' => $board, 'boardParams' => $boardParms]
        );

        $this->returnPDFResponseFromHTML($html);
    }

    private function initializeBoard()
    {
        $board = [];
        for ($x = 1; $x <= 10; $x++) {
            for ($y = 1; $y <= 10; $y++) {
                $board[$x][$y] = null;
            }
        }
        return $board;
    }

    private function returnPDFResponseFromHTML(string $html)
    {
        $pdf = $this->container->get("white_october.tcpdf")->create(
            'PORTRAIT',
            PDF_UNIT,
            PDF_PAGE_FORMAT,
            true,
            'UTF-8',
            false
        );
        $pdf->SetAuthor('Paweł Zelant');
        $pdf->SetTitle('Warcaby');
        $pdf->SetSubject('Wydruk planszy');
        $pdf->SetKeywords('Warcaby');
        $pdf->setFontSubsetting(true);

        $pdf->SetFont('helvetica', '', 11, '', true);
        $pdf->AddPage();

        $pdf->writeHTMLCell(
            $w = 0,
            $h = 0,
            $x = '',
            $y = '',
            $html,
            $border = 0,
            $ln = 1,
            $fill = 0,
            $reseth = true,
            $align = '',
            $autopadding = true
        );

        $pdf->Output("warcaby.pdf", 'I');
    }

    private function parsePositions(array $board, $positions)
    {
        foreach ($positions as $checker) {
            $board[$checker[0]][$checker[1]] = $checker[2] . (!empty($checker[3]) ? '_king' : null);
        }
        return $board;
    }

    private function parseBoardParams($paramObj)
    {
        return [
            'playerWhiteName' => $paramObj->playerWhiteName,
            'playerBlackName' => $paramObj->playerBlackName,
            'beatenCountBlack' => $paramObj->beatenCountBlack,
            'beatenCountWhite' => $paramObj->beatenCountWhite,
            'actualTurn' => $paramObj->actualTurn
        ];
    }
}